package org.example.book.api;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.example.book.persistence.entity.Book;
import org.example.book.persistence.entity.BookStore;
import org.example.book.persistence.repository.BookRepository;
import org.example.book.persistence.repository.BookStoreRepository;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReactiveApiResource {

    @Inject
    BookRepository bookRepository;
    @Inject
    BookStoreRepository bookStoreRepository;

    @GET
    @Path("/books")
    public Uni<Response> getBooks() {
        return bookRepository
                .getBookList();
    }

    @POST
    @Path("/books")
    public Uni<Response> addBook(final Book book) {
        return bookRepository
                .addBook(book);
    }

    @POST
    @Path("/bookStores")
    public Uni<Response> addBookStore(final BookStore bookStore) {
        return bookStoreRepository
                .addBookStore(bookStore);
    }

    @POST
    @Path("/bookStores/{bookStoreId}/book/{bookId}")
    public Uni<Response> addBookToBookStore(@PathParam("bookId") final String bookId,
                                            @PathParam("bookStoreId") final String bookStoreId) {
        return bookStoreRepository
                .addBookToBookStore(bookId, bookStoreId);
    }

    @GET
    @Path("/bookStores")
    public Uni<Response> getBookStores() {
        return bookStoreRepository
                .getBookStoreList();
    }

    @PUT
    @Path("/books/{bookId}")
    public Uni<Response> updateBook(@PathParam("bookId") String id,
                                    final Book book) {
        return bookRepository
                .updateBook(book, id);
    }

    @DELETE
    @Path("/books/{bookId}")
    public Uni<Response> deleteBook(@PathParam("bookId") String id) {
        return bookRepository.deleteBook(id);
    }


}
