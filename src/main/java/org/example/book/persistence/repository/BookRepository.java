package org.example.book.persistence.repository;

import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;
import org.example.book.persistence.entity.Award;
import org.example.book.persistence.entity.Book;
import org.example.book.util.Constants;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@ApplicationScoped
@WithTransaction
public class BookRepository implements PanacheRepository<Book> {

    public Uni<Response> getBookList() {
        return listAll(Sort.by("title"))
                .onItem()
                .transformToUni(book -> Uni.createFrom().item(Response.ok(book)::build))
                .ifNoItem()
                .after(Duration.ofMillis(Constants.AWAITING_TIME))
                .fail()
                .onFailure()
                .recoverWithUni(Uni.createFrom().item(Response.status(500).entity(Collections.emptyList()).build()));
    }

    public Uni<Response> addBook(final Book book) {
        book.setAward(getAwards());
        return persist(book)
                .replaceWith(Response.ok(book)::build)
                .ifNoItem()
                .after(Duration.ofMillis(Constants.AWAITING_TIME))
                .fail()
                .onFailure()
                .transform(t -> new IllegalStateException("Error"));
    }

    public Uni<Response> updateBook(final Book book,
                                    final String id) {
        return update("author = :author , publisher = :publisher , title = :title , category = :category where id = :id",
                Map.of("author", book.getAuthor(),
                        "publisher", book.getPublisher(),
                        "title", book.getTitle(),
                        "category", book.getCategory(),
                        "id", id))
                .onItem()
                .transformToUni(result -> Uni.createFrom().item(Response.accepted(result)::build))
                .ifNoItem()
                .after(Duration.ofMillis(Constants.AWAITING_TIME))
                .fail()
                .onFailure()
                .transform(t -> new IllegalStateException("Error"));
    }

    public Uni<Response> deleteBook(final String id) {
        return deleteById(Long.valueOf(id))
                .onItem()
                .transformToUni(result -> Uni.createFrom().item(Response.ok(result)::build))
                .ifNoItem()
                .after(Duration.ofMillis(Constants.AWAITING_TIME))
                .fail()
                .onFailure()
                .transform(t -> new IllegalStateException("Error"));
    }

    private List<Award> getAwards() {
        return List.of(new Award("1", Instant.now(), 1),
                new Award("2", Instant.now(), 2),
                new Award("3", Instant.now(), 3),
                new Award("4", Instant.now(), 4),
                new Award("5", Instant.now(), 5));
    }

}
