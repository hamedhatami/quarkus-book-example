package org.example.book.persistence.repository;

import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import org.example.book.persistence.entity.BookStore;
import org.example.book.util.Constants;
import org.hibernate.reactive.mutiny.Mutiny;

import java.time.Duration;
import java.util.Collections;

@ApplicationScoped
@WithTransaction
public class BookStoreRepository implements PanacheRepository<BookStore> {

    @Inject
    BookRepository bookRepository;

    public Uni<Response> getBookStoreList() {
        return listAll(Sort.by("name"))
                .onItem()
                .transformToUni(bookStores -> Uni.createFrom().item(Response.ok(bookStores)::build))
                .ifNoItem()
                .after(Duration.ofMillis(Constants.AWAITING_TIME))
                .fail()
                .onFailure()
                .recoverWithUni(Uni.createFrom().item(Response.status(500).entity(Collections.emptyList()).build()));
    }

    public Uni<Response> addBookStore(final BookStore bookStore) {
        return persist(bookStore)
                .replaceWith(Response.ok(bookStore)::build)
                .ifNoItem()
                .after(Duration.ofMillis(Constants.AWAITING_TIME))
                .fail()
                .onFailure()
                .transform(t -> new IllegalStateException("Error"));
    }

    public Uni<Response> addBookToBookStore(final String bookId,
                                            final String bookStoreId) {
        return find("id", bookStoreId)
                .singleResult()
                .call(bookStore -> Mutiny.fetch(bookStore.getBooks()))
                .chain(bookStore -> bookRepository.find("id", bookId)
                        .singleResult()
                        .chain(book -> {
                            bookStore.getBooks().add(book);
                            return Uni.createFrom().item(bookStore);
                        }))
                .chain(this::persist)
                .replaceWith(Response.ok("{\"assigned\":\"true\"}")::build);
    }
}
