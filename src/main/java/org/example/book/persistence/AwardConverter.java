package org.example.book.persistence;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.example.book.persistence.entity.Award;

import java.util.ArrayList;
import java.util.List;

import static org.example.book.util.Constants.JSON_B;

@Converter(autoApply = true)
public class AwardConverter implements AttributeConverter<List<Award>, String> {
    @Override
    public String convertToDatabaseColumn(List<Award> awards) {
        return JSON_B.toJson(awards);
    }

    @Override
    public List<Award> convertToEntityAttribute(String data) {
        return JSON_B.fromJson(data, new ArrayList<Award>() {
        }.getClass().getGenericSuperclass());
    }
}
