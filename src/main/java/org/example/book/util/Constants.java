package org.example.book.util;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;

@UtilityClass
public class Constants {
    public static final Jsonb JSON_B = JsonbBuilder.create(new JsonbConfig()
            .withNullValues(false)
            .withEncoding(StandardCharsets.UTF_8.name()));
    public static final int AWAITING_TIME = 200;
}
